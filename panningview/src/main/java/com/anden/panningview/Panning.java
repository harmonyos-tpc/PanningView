package com.anden.panningview;

import ohos.agp.utils.RectFloat;

/**
 * Base class to implement a scrolling animation
 * that can be set in @{@link PanningView#setPanning(Panning)}
 */
public abstract class Panning {
    private RectFloat displayRect;
    private RectFloat viewRect;
    private float xStartOffset = 0f;
    private float xEndOffset = 0f;
    private Interpolator INTERPOLATOR = new LinearInterpolator();
    private Interpolator interpolator = INTERPOLATOR;

    /**
     * Print info log
     *
     * @param drawableRect RectFloat tag
     * @param viewRect     RectFloat message
     */
    public void setSize(RectFloat drawableRect, RectFloat viewRect) {
        this.displayRect = drawableRect;
        this.viewRect = viewRect;
    }

    /**
     * Returns a x value in elapsed fraction of an animation
     *
     * @param dt elapsed fraction (elapsedTime / duration)
     * @return float
     */
    public abstract float getX(float dt);

    /**
     * Returns a y value in elapsed fraction of an animation
     *
     * @param dt elapsed fraction (elapsedTime / duration)
     * @return float
     */
    public abstract float getY(float dt);

    /**
     * getDisplaySize
     *
     * @return react
     */
    public RectFloat getDisplaySize() {
        return displayRect;
    }

    /**
     * getViewSize
     *
     * @return react
     */
    public RectFloat getViewSize() {
        return viewRect;
    }

    /**
     * getStartXOffset
     *
     * @return react
     */
    public float getStartXOffset() {
        return this.xStartOffset;
    }

    /**
     * getEndXOffset
     *
     * @return react
     */
    public float getEndXOffset() {
        return this.xStartOffset + this.xEndOffset;
    }

    /**
     * getInterpolator
     *
     * @return interpolator
     */
    public Interpolator getInterpolator() {
        return interpolator;
    }
}
