package com.anden.panningview;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import ohos.agp.utils.RectFloat;

/**
 * HorizontalPanning
 */
public class HorizontalPanning extends Panning {
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({RIGHT_TO_LEFT, LEFT_TO_RIGHT})
    public @interface Way {
    }

    /**
     * RIGHT_TO_LEFT
     */
    public static final int RIGHT_TO_LEFT = 0;

    /**
     * LEFT_TO_RIGHT
     */
    public static final int LEFT_TO_RIGHT = 1;

    private int way;

    private float startX;
    private float endX;

    public HorizontalPanning(@Way int way) {
        this.way = way;
    }

    @Override
    public void setSize(RectFloat drawableRect, RectFloat viewRect) {
        super.setSize(drawableRect, viewRect);
        this.startX = calculateStartX();
        this.endX = calculateEndX();
    }

    private float calculateStartX() {
        RectFloat displayRect = getDisplaySize();
        RectFloat viewRect = getViewSize();
        if (way == RIGHT_TO_LEFT) {
            return displayRect.left + (viewRect.getWidth() * getStartXOffset());
        }
        return (viewRect.getWidth() - displayRect.right) - (viewRect.getWidth() * getStartXOffset());
    }

    private float calculateEndX() {
        RectFloat displayRect = getDisplaySize();
        RectFloat viewRect = getViewSize();
        if (way == RIGHT_TO_LEFT) {
            return viewRect.getWidth() - (displayRect.right + (viewRect.getWidth() * getEndXOffset()));
        }
        return (viewRect.getWidth() * getEndXOffset()) + (displayRect.right - viewRect.getWidth());
    }

    @Override
    public float getX(float dt) {
        return startX + getInterpolator().getInterpolation(dt) * endX;
    }

    @Override
    public float getY(float dt) {
        return 0;
    }
}
