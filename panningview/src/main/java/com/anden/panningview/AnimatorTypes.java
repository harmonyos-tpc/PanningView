/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anden.panningview;

/**
 * AnimatorTypes.
 */
public class AnimatorTypes {
    /**
     * MOVE_BY_X.
     */
    public static final int MOVE_BY_X = 1;

    /**
     * MOVE_TO_X.
     */
    public static final int MOVE_TO_X = 2;

    /**
     * MOVE_FROM_X.
     */
    public static final int MOVE_FROM_X = 3;
    /**
     * MOVE_BY_Y.
     */
    public static final int MOVE_BY_Y = 4;

    /**
     * MOVE_TO_Y.
     */
    public static final int MOVE_TO_Y = 5;

    /**
     * MOVE_FROM_Y.
     */
    public static final int MOVE_FROM_Y = 6;

    /**
     * ALPHA.
     */
    public static final int ALPHA = 7;

    /**
     * ROTATE.
     */
    public static final int ROTATE = 8;

    /**
     * SCALE_X.
     */
    public static final int SCALE_X = 9;

    /**
     * SCALE_X_BY.
     */
    public static final int SCALE_X_BY = 10;
    /**
     * SCALE_Y.
     */
    public static final int SCALE_Y = 11;

    /**
     * SCALE_Y_BY.
     */
    public static final int SCALE_Y_BY = 12;

    /**
     * checkAnimatorType.
     * @param animatorType int
     * @return boolean
     */
    public static boolean checkAnimatorType(int animatorType) {
        switch (animatorType) {
            case MOVE_BY_X:
            case MOVE_TO_X:
            case MOVE_FROM_X:
            case MOVE_BY_Y:
            case MOVE_TO_Y:
            case MOVE_FROM_Y:
            case ALPHA:
            case ROTATE:
            case SCALE_X:
            case SCALE_X_BY:
            case SCALE_Y:
            case SCALE_Y_BY:
                return true;
            default:
                return false;
        }
    }
}
