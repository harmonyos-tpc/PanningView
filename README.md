# PanningView
  PanningView: An openharmony library for panning an image horizontal and vertically.
 
## PanningView includes:
* PanningView provides a view that can animate background drawable. It's provide a horizontal and vertical panning
* it includes the train moving from left to right and updating the panning
* it includes custom animation to improve the horizontal and vertical panning
* implemented custom animation for moving the train drawable panning right and top.

## Usage Instructions
1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.

2. The following core classes are the essential interface to PanningView:
PanningView: The entry point for using EnumButton panning holds the  train drawable image to move the panning right and top postion.
PanningAnimatorValue: Manages the top and right animation in panningview .

3. The steps to initialize the PanningView and the core Animation classes:
	* Component declaration in layout
          <com.anden.panningview.PanningView
          ohos:id="$+id:train"
          ohos:height="match_content"
          ohos:width="match_parent"
          ohos:align_parent_bottom="true"
          ohos:bottom_margin="30vp"
          app:autoStart="true"
          app:drawable="$media:subway"
          app:duration="3000"/>

    * Apply the panning animation in Ablity.
		  subway = (PanningView)findComponentById(ResourceTable.Id_train);
          start = (Button) findComponentById(ResourceTable.Id_start);
            start.setClickedListener(component -> {
                       if (isClickable.equalsIgnoreCase("panelUpdated")) {
                           ToastUtils.showToast(MainAbility.this, "Panning Start");
                           if (rope != null) {
                               rope.stop(true);
                               rope.clear(true);
                           }
                           EnumButton technique = EnumButton.SlideHorizontal;
                           rope = AnimPanel.with(technique)
                                   .duration(duration)
                                   .playOn(subway);
                           updatePanelToast();
                       } else {
                           ToastUtils.showToast(MainAbility.this, "Panning Start");
                           if (rope != null) {
                               rope.stop(true);
                               rope.clear(true);
                           }
                           EnumButton technique = EnumButton.SlideVertical;
                           rope = AnimPanel.with(technique)
                                   .duration(duration)
                                   .playOn(subway);
                       }
                       updatePanelToast();
                   });
                   horizontal.setClickedListener(new Component.ClickedListener() {
                       @Override
                       public void onClick(Component component) {
                           isClickable = "panelUpdated";
                       }
                   });
                   vertical.setClickedListener(new Component.ClickedListener() {
                       @Override
                       public void onClick(Component component) {
                           isClickable = "";
                       }
                   });
                   resume.setClickedListener(component -> {
                       if (rope != null) {
                           rope.resume(true);
                       }
                   });
                   pause.setClickedListener(component -> {
                       if (rope != null) {
                           rope.pause(true);
                       }
                   });
               }

               private void updatePanelToast() {
                   getUITaskDispatcher().delayDispatch(new Runnable() {
                       @Override
                       public void run() {
                           ToastUtils.showToast(MainAbility.this, "Panning Finish");
                       }
                   },duration);
               }

 ## Installation instruction

Method 1:

For using panningview module in sample app,include the below library dependency to generate hap/panningview.har:
Add the dependencies in entry/build.gradle as below :

    dependencies {
      implementation project(path: ':panningview')
      }

Method 2:

 Using the panningview har, make sure to add panningview.har file in the entry/libs folder and add the below dependency
in build.gradle.
Modify the dependencies in the entry/build.gradle file.

  dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 }
 
 Method 3:
 
 For using PanningView from a remote repository in separate application, add the below dependency in entry/build.gradle file.
 
 Modify entry build.gradle as below :
             
             
             ```gradle
             dependencies {
                    implementation 'io.openharmony.tpc.thirdlib:PanningView:1.0.0'
             }
             ```

 ## License
Distributed under the Apache license. See LICENSE for more information.

