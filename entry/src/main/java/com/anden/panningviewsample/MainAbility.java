/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anden.panningviewsample;

import com.anden.panningview.AnimPanel;
import com.anden.panningview.PanningView;
import com.anden.panningview.EnumButton;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbility.
 */
public class MainAbility extends Ability {
    private PanningView subway;
    private Button vertical;
    private Button horizontal;
    private Button start;
    private Button pause;
    private AnimPanel.YoYoString rope;
    private Button resume;
    private long duration = 2000;
    private String isClickable = "panelUpdated";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        if (findComponentById(ResourceTable.Id_train) instanceof PanningView) {
            subway = (PanningView) findComponentById(ResourceTable.Id_train);
        }
        if (findComponentById(ResourceTable.Id_start) instanceof Button) {
            start = (Button) findComponentById(ResourceTable.Id_start);
        }
        if (findComponentById(ResourceTable.Id_horizontal) instanceof Button) {
            horizontal = (Button) findComponentById(ResourceTable.Id_horizontal);
        }
        if (findComponentById(ResourceTable.Id_vertical) instanceof Button) {
            vertical = (Button) findComponentById(ResourceTable.Id_vertical);
        }
        if (findComponentById(ResourceTable.Id_resume) instanceof Button) {
            resume = (Button) findComponentById(ResourceTable.Id_resume);
        }
        if (findComponentById(ResourceTable.Id_pause) instanceof Button) {
            pause = (Button) findComponentById(ResourceTable.Id_pause);
        }
        getData();
    }

    private void getData() {
        start.setClickedListener(component -> {
            if (isClickable.equalsIgnoreCase("panelUpdated")) {
                ToastUtils.showToast(MainAbility.this, "Panning Start");
                if (rope != null) {
                    rope.stop(true);
                    rope.clear(true);
                }
                EnumButton technique = EnumButton.SlideHorizontal;
                rope = AnimPanel.with(technique)
                        .duration(duration)
                        .playOn(subway);
                updatePanelToast();
            } else {
                ToastUtils.showToast(MainAbility.this, "Panning Start");
                if (rope != null) {
                    rope.stop(true);
                    rope.clear(true);
                }
                EnumButton technique = EnumButton.SlideVertical;
                rope = AnimPanel.with(technique)
                        .duration(duration)
                        .playOn(subway);
            }
            updatePanelToast();
        });
        horizontal.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                isClickable = "panelUpdated";
            }
        });
        vertical.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                isClickable = "";
            }
        });
        resume.setClickedListener(component -> {
            if (rope != null) {
                rope.resume(true);
            }
        });
        pause.setClickedListener(component -> {
            if (rope != null) {
                rope.pause(true);
            }
        });
    }

    private void updatePanelToast() {
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showToast(MainAbility.this, "Panning Finish");
            }
        },duration);
    }
}

